"use client";

import { useState } from "react";
import { v4 as uuidv4 } from "uuid";

enum ACTIONS {
  ADD = "add",
  MINUS = "minus",
}

const defaultOperation = { operation: ACTIONS.ADD, value: "0", isDisabled: false };

function App() {
  const [operations, setOperations] = useState([{ id: uuidv4(), ...defaultOperation }]);

  const onChangeOperation = (id: string, operation: ACTIONS) => {
    const index = operations.findIndex((operation) => operation.id === id);

    const newOperations = [...operations];
    newOperations[index] = { ...operations[index], operation: operation as ACTIONS };

    setOperations(newOperations);
  };

  const onChangeValue = (id: string, value: string) => {
    const index = operations.findIndex((operation) => operation.id === id);

    const newOperations = [...operations];
    newOperations[index] = { ...operations[index], value };

    setOperations(newOperations);
  };

  const onDeleteOperation = (id: string) => {
    const index = operations.findIndex((operation) => operation.id === id);

    const newOperations = [...operations];

    newOperations.splice(index, 1);

    setOperations(newOperations);
  };

  const result = operations.reduce((accumulator: any, currentValue: any) => {
    if (!currentValue.isDisabled) {
      if (currentValue.operation === ACTIONS.ADD) {
        return accumulator + Number(currentValue.value);
      }

      if (currentValue.operation === ACTIONS.MINUS) {
        return accumulator - Number(currentValue.value);
      }
    }

    return accumulator;
  }, 0);

  const addOperation = () => {
    const newOperations = [...operations];

    newOperations.push({ id: uuidv4(), ...defaultOperation });

    setOperations(newOperations);
  };

  const disableOperation = (id: string) => {
    const index = operations.findIndex((operation) => operation.id === id);

    const newOperations = [...operations];
    newOperations[index] = { ...operations[index], isDisabled: !operations[index].isDisabled };

    setOperations(newOperations);
  };

  return (
    <div className="flex flex-col items-center justify-center min-h-[100vh]">
      <h1 className="mb-8 text-xl font-bold">My Calculator</h1>

      <button onClick={addOperation} className="bg-blue-500 p-2 rounded-xl text-white mb-6">
        Add row
      </button>

      <ul>
        {operations.map(({ id, operation, value, isDisabled }) => (
          <li key={id} className="flex gap-8 mb-4">
            <select
              value={operation}
              onChange={(e) => onChangeOperation(id, e.target.value as ACTIONS)}
              className="w-16"
            >
              <option selected value={ACTIONS.ADD}>
                +
              </option>

              <option value={ACTIONS.MINUS}>-</option>
            </select>

            <input
              type="number"
              value={value}
              onChange={(e) => onChangeValue(id, e.target.value)}
            />

            <div className="flex gap-6">
              <button onClick={() => onDeleteOperation(id)}>Delete</button>

              <button onClick={() => disableOperation(id)}>
                {isDisabled ? "Enable" : "Disable"}
              </button>
            </div>
          </li>
        ))}
      </ul>

      <dl className="flex gap-2">
        <dt>Result:</dt>
        <dd>{result}</dd>
      </dl>
    </div>
  );
}

export default App;
